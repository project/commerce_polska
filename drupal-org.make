; Drupal.org release file.
core = 7.9
api = 2

projects[addressfield] = 1.0-beta2
projects[admin_menu] = 3.0-rc1
projects[commerce] = 1.0
projects[ctools] = 1.0-rc1
projects[entity] = 1.0-beta11
projects[google_analytics] = 1.2
projects[pathauto] = 1.0-rc2
projects[rules] = 2.0
projects[simplenews] = 1.0-alpha1
projects[token] = 1.0-beta6
projects[views] = 3.0-rc1
